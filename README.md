# Experiments! #

A re-sorting of [this repository on my team account](https://bitbucket.org/xoio/xoio.bitbucket.org) as I've been following a more clean way of organizing things.

### What is this repository for? ###

Source code for various things I have around the web


How do I get set up? 
===
__For all folders not marked with `-cljs`__

* Source files will be handled by npm scripts. 
* type `npm start` to init watchify
* open up another terminal and run `python -m SimpleHTTPServer <optional port number>` from the `public` directory and navigate to page in your browser 

__For all folders marked with `-cljs`__

* Most of the time, I'll be using [leiningn](http://leiningen.org/) with figwheel (but might occasionally use boot)
* if `lein figwheel` doesn't work, then run `boot dev` to start up boot in development mode
*  Lein will automatically spin up a server at `localhost:3449` and boot will start a server at `localhost:3000`