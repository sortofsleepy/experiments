import Capabilities from "./src/framework/Capabilities"
import EnvMap from "./src/EnvMap"
import ParticleSystem from "./src/ParticleSystem"
import RibbonSystem from "./src/RibbonSystem"
import Ribbon from "./src/Ribbon"
import raf from 'raf'


if(Capabilities.hasWebGL()){
    var renderer = new THREE.WebGLRenderer();
    renderer.autoClear = true;
    renderer.setClearColor(0xffffff,1);
    renderer.setSize( window.innerWidth, window.innerHeight );
    renderer.setPixelRatio( window.devicePixelRatio );
    document.body.appendChild( renderer.domElement );

    var scene = new THREE.Scene();

    var camera = new THREE.PerspectiveCamera( 60, window.innerWidth / window.innerHeight, .1, 10000 );
    camera.position.z = 200;

    var controls = new THREE.OrbitControls( camera, renderer.domElement );
    var clock = new THREE.Clock();
    var resolution = new THREE.Vector2( window.innerWidth, window.innerHeight );
    var graph = new THREE.Object3D();
    scene.add( graph );

    var system = new RibbonSystem(renderer,camera);
    system.addTo(scene);

    var system2 = new ParticleSystem(renderer);
    system2.addTo(scene);



    raf(function tick(){
        system.update();
        system2.update();
        renderer.render(scene,camera);

        raf(tick);
    })

}
