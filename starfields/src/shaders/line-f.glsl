#extension GL_OES_standard_derivatives : enable
precision mediump float;
uniform sampler2D reflective;
uniform sampler2D map;
uniform float useMap;
uniform float useDash;
uniform vec2 dashArray;

varying vec2 vUV;
varying vec4 vColor;
varying vec3 vPosition;

void main() {
    vec3 pos = vPosition;
    vec4 c = vColor;
    vec4 reflect = texture2D(reflective,vUV);

    if( useMap == 1. ) c *= texture2D( map, vUV );
	 if( useDash == 1. ){

	 }
	 gl_FragColor =  reflect;
    //gl_FragColor = c;


}