precision mediump float;
precision mediump int;
uniform mat4 modelViewMatrix; // optional
uniform mat4 projectionMatrix; // optional
attribute vec3 position;
attribute float particleSize;

uniform float time;
varying vec3 vPosition;
uniform sampler2D map;

void main()	{
    vec4 pos = texture2D( map , position.xy );
    vPosition = position;



     vec4 poss = vec4( pos.xyz, 1.0 );

    gl_PointSize = 5.0;

    gl_Position = projectionMatrix * modelViewMatrix * poss;

}