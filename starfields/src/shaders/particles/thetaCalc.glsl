uniform sampler2D originTexture;
uniform sampler2D destinationTexture;
uniform sampler2D thetaSpeed;
uniform float dT;
uniform float noiseSize;
uniform vec2  resolution;

varying vec2 vUv;


void main(){

  vec2 uv = gl_FragCoord.xy / resolution;
  vec4 oPos = texture2D( originTexture , uv );
  vec4 pos  = texture2D( destinationTexture , uv );
  vec4 thetaSpeedVal  = texture2D( thetaSpeed , uv );
  pos += thetaSpeedVal * 0.005;
  gl_FragColor = pos;
}
