precision mediump float;
precision mediump int;

uniform vec2 resolution;
uniform float time;
varying vec3 vPosition;
uniform sampler2D noise;
uniform sampler2D pTex;

void main()	{
    vec2 uv = gl_PointCoord / resolution;

    vec4 color = vec4( 1. );
    color= vec4(vPosition,1.0);

    vec4 particleTexture = texture2D(pTex,gl_PointCoord);

    gl_FragColor =  color + particleTexture;

}
