uniform sampler2D originTexture;
uniform sampler2D destinationTexture;

void main(){

    vec4 pos = texture2D(originTexture,uv);
    vec4 dst = texture2D(destinationTexture,uv);

    gl_FragColor = pos;
}