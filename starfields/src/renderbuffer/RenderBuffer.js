/**
 * A class for doing GPU pingponging.
 * Largely based on Isaac Cohen's PhysicsRenderer(https://github.com/cabbibo), cleaned up a bit, and
 * adding some flags for the future so this can (hopefully) be used interchangeably with
 * regular WebGL code in addition to Three.js
 *
 * Requires nitro-glutils
 */

import GLUtils from "./GLUtils.js"
import Capabilities from "../framework/Capabilities"
class RenderBuffer {
    constructor(size , shader , renderer , additionalUniforms) {

        this.renderer = renderer;

        this.size = size !== undefined ? size : 128;
        this.s2   = size * size;

        this.renderer = renderer;

        this.clock = new THREE.Clock();

        this.resolution = new THREE.Vector2( this.size , this.size );
        this.counter = 0;

        this.texturePassProgram = GLUtils.createPassthru();

        //! a flag to help aid in determining how many render targets to create.
        this.dataSet = false;

        //! a flag to indicate whether or not render targets have been built.
        this.fbosBuilt = false;

        // WHERE THE MAGIC HAPPENS
        var uniforms = this._buildUniforms(additionalUniforms);

        this.simulation = GLUtils.generateShader('passthru',shader,uniforms);
        this.simulationMaterial = this.simulation;

        //build scene for target rendering
        this._buildRenderScene();

    }

    run(){
        //update default uniforms
        this.simulationMaterial.uniforms.timer.value += 0.01;

        /**
         * If we've set some base data,
         * we run it through all of the render targets
         * keeping track of the previous result.
         *
         * If we haven't, we only utilize one target.
         */
        if(this.dataSet){
            this.update();
        }else{

            //quickly build out a render target
            if(this.counter < 1){
                this._buildRenderTargets();
                this.counter++;
            }

            //1 update the simulation and render the result in a target texture
            this.renderer.render( this.scene, this.camera, this.rtt, true );

            //set the output so we can grab the result in the rendering shader.
            this.output = this.rtt;
        }
    }

    /**
     * Updates the simulation
     */
    update(){
        var flipFlop = this.counter % 3;

        if( flipFlop == 0 ){
            this.simulation.uniforms.destinationTexture.value = this.rt_2;
            this.pass( this.simulation, this.rt_3 );

        }else if( flipFlop == 1 ){
            this.simulation.uniforms.destinationTexture.value = this.rt_3;
            this.pass( this.simulation , this.rt_1 );
        }else if( flipFlop == 2 ){
            this.simulation.uniforms.destinationTexture.value = this.rt_1;
            this.pass( this.simulation , this.rt_2 );
        }

        this.counter ++;

    }

    /**
     * Runs a pass against the current target. Sets the output target for use
     * in rendering
     * @param shader the shader to use
     * @param target the target to run the pass on
     *
     * TODO remember to take out the shader param as we don't really need it.
     */
    pass(shader,target){
        this.mesh.material = shader;

        this.renderer.render( this.scene, this.camera, target, false );
        this.output = target;
    }

    /**
     * Gets the current output of the simulation.
     * @returns {*}
     */
    getOutput(){
        if(this.output !== undefined){
            return this.output;
        }
    }

    /**
     * Set a additional uniform variable in the simulation
     * @param uniform name of hte uniform
     * @param value the value for the nuniform
     */
    setUniform(uniform,value){
        if(THREE){
            this.simulation.uniforms[uniform] = {
                type:determineType(value),
                value:value
            };

            function determineType(value){
                var instance = null;

                if(value instanceof THREE.Vector2){
                    instance = 'v2';
                } else if(value instanceof THREE.Vector3){
                    instance = 'v3';
                }else if((value instanceof THREE.Texture)){
                    instance = 't';
                }else if(value instanceof THREE.WebGLRenderTarget){
                    instance = 't';
                }else{
                    instance = 'f';
                }

                return instance;
            }
        }
    }

    updateUniform(uniform,value){
        try{
            this.simulation.uniforms[uniform].value = value;
        }catch(e){}
    }

    setRandomOriginData(size,alpha){
        this.resetRand(size,alpha)
    }

    /**
     * Kicks things off with a bit of random data.
     * @param size
     * @param alpha
     */
    resetRand(size,alpha){

        if(THREE){
            var size = size || 100;
            var data = new Float32Array( this.s2 * 4 );

            for( var i =0; i < data.length; i++ ){

                //console.log('ss');
                data[ i ] = (Math.random() - .5 ) * size;

                if( alpha && i % 4 ===3 ){
                    data[i] = 0;
                }

            }

            var texture = new THREE.DataTexture(
                data,
                this.size,
                this.size,
                THREE.RGBAFormat,
                THREE.FloatType
            );

            texture.minFilter =  THREE.NearestFilter,
                texture.magFilter = THREE.NearestFilter,

                texture.needsUpdate = true;

            this.setData( texture);
        }
    }

    ////////////////// INTERNAL /////////////////////////

    _buildUniforms(uniforms){
        var _uniforms = {
            originTexture:{
                type:"t",
                value:null
            },
            destinationTexture:{
                type:"t",
                value:null
            },
            resolution:{
                type:"v2" ,
                value: this.resolution
            },
            timer:{
                type:'f',
                value:0.0
            }
        };

        if(uniforms !== undefined){
            Object.assign(_uniforms,uniforms);
        }
        return _uniforms;
    }

    /**
     * Sets the data to be used for the simulation and runs a pass across all the buffers.
     * @param texture
     */
    setData(texture){
        this.texture = texture;
        this.texturePassProgram.uniforms.texture.value = texture;

        this.dataSet = true;

        this._buildRenderTargets();

        this.pass( this.texturePassProgram , this.rt_1 );
        this.pass( this.texturePassProgram , this.rt_2 );
        this.pass( this.texturePassProgram , this.rt_3 );
    }

    _buildRenderScene(){
        if(THREE){

            var camera = new THREE.OrthographicCamera( - 0.5, 0.5, 0.5, - 0.5, 0, 1 );
            var scene = new THREE.Scene();
            var mesh = new THREE.Mesh( new THREE.PlaneBufferGeometry( 1, 1 ),this.simulationMaterial );

            var size = this.size;
            scene.add(mesh);

            this.mesh = mesh;
            this.camera = camera;
            this.scene = scene;
        }
    }

    _buildRenderTargets(){
        if(Capabilities.haveThreejs()){
            if(this.dataSet){
                // Sets up our render targets
                this.rt_1 = GLUtils.generateRenderTarget(this.size,this.size);
                this.rt_2 = this.rt_1.clone();
                this.rt_3 = this.rt_1.clone();
            }else{

                this.rtt = GLUtils.generateRenderTarget(this.size,this.size);
            }

        }else{
            this.rt_1 = this.renderer.createFramebuffer();
            this.rt_2 = this.renderer.createFramebuffer();
            this.rt_3 = this.renderer.createFramebuffer();
        }

        this.fbosBuilt = true;
    }
}

export default RenderBuffer;