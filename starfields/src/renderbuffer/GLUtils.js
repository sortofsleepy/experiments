import Capabilities from "../framework/Capabilities"

class GLUtils {
    constructor(){}

    /**
     * Checks for the existance of a WebGL extension.
     * @param extensionName the name of the extension to check for
     * @param context a webgl rendering context. You only need to pass one in
     * if you did not run {@link Capabilities#canWebGL}, otherwise will use the assigned global context
     * @returns {boolean}
     */
    static checkExtension(extensionName,context=null){
        let gl = window.globalContext;
        let check = "";
        if(gl !== undefined){
            check = gl.getExtension(extensionName);
        }else if(context !== null){
            check = context.getExtension(extensionName);
        }else{
            console.error("Unable to check for extension due to a lack of a WebGLRenderingContext");
        }

        if((check !== null) ||(check !== "")){
            return true;
        }else{
            return false;
        }
    }
    /**
     *	Creates a passthru shader
     */
    static createPassthru(buildType){
        this.passThruVS =[
            'varying vec2 vUv;',
            'void main() {',
            '  vUv = uv;',
            '  gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );',
            '}'
        ].join('\n');

        this.passThruFS = [
            'uniform sampler2D texture;',
            'varying vec2 vUv;',
            'void main() {',
            '  vec2 read = vUv;',
            '  vec4 c = texture2D( texture , vUv );',
            '  gl_FragColor = c ;',
            //   'gl_FragColor = vec4(1.);',
            '}'
        ].join('\n');

        if(Capabilities.haveThreejs()){
            var uniforms = {
                texture:{  type:'t'  , value:null }
            };

            var texturePassShader = new THREE.ShaderMaterial({
                uniforms:uniforms,
                vertexShader:this.passThruVS,
                fragmentShader:this.passThruFS
            });
            return texturePassShader;
        }


    }
    static generateRenderTarget(width,height,options){
        if(Capabilities.haveThreejs()){

            var defaults = {
                minFilter: THREE.NearestFilter,
                magFilter: THREE.NearestFilter,
                format: THREE.RGBAFormat,
                type: THREE.FloatType,
                stencilBuffer: false
            };

            return new THREE.WebGLRenderTarget(width,height,defaults);
        }
    }

    //alias for {@link GLUtils#generateShader}
    static createShader(vSource,fSource,uniforms,context){
        return GLUtils.generateShader(vSource,fSource,uniforms,context);
    }

    /**
     * Generates a Shader program
     * @param vSource the source for the vertex shader
     * @param fSource the source for the fragment shader
     * @param uniforms any additional uniforms you want embeded in the shader
     * @param context a WebGLRendering context when not using threejs
     * @returns {THREE.ShaderMaterial}
     */
    static generateShader(vSource,fSource,uniforms,context){
        if(Capabilities.haveThreejs()){
            var vertex = "";
            var _uniforms = {
                time:{
                    type:'f',
                    value:0.0
                },
                mousePos:{
                    type:'v2',
                    value:new THREE.Vector2()
                }
            }

            if(uniforms !== undefined){
                Object.assign(_uniforms, uniforms);
            }

            if(vSource === "passthru"){
                vertex = GLUtils.createPassthruVertex();
            }

            return new THREE.ShaderMaterial({
                vertexShader:vertex,
                fragmentShader:fSource,
                uniforms:uniforms
            })

        }else{
            let gl = context;
            var vertexShader = gl.createShader( gl.VERTEX_SHADER );
            var fragmentShader = gl.createShader( gl.FRAGMENT_SHADER );

            gl.shaderSource( vertexShader, vSource);

            gl.shaderSource( fragmentShader, fSource );

            gl.compileShader( vertexShader );
            if (!gl.getShaderParameter(vertexShader, gl.COMPILE_STATUS)) {
                console.error("Shader failed to compile", gl.getShaderInfoLog( vertexShader ));
                return null;
            }

            gl.compileShader( fragmentShader );
            if (!gl.getShaderParameter(fragmentShader, gl.COMPILE_STATUS)) {
                console.error("Shader failed to compile", gl.getShaderInfoLog( fragmentShader ));
                return null;
            }

            var program = gl.createProgram();

            gl.attachShader( program, vertexShader );
            gl.attachShader( program, fragmentShader );

            gl.deleteShader( vertexShader );
            gl.deleteShader( fragmentShader );

            gl.linkProgram( program );

            if (!gl.getProgramParameter(program, gl.LINK_STATUS)) {
                console.error("Shader program failed to link", gl.getProgramInfoLog( program ));
                gl.deleteProgram(program);
                return null;
            }

            return program;
        }
    }


    /**
     * Generates a Fbo (or a THREE.WebGLRenderTarget when using Three.js)
     * @param width the width for the fbo
     * @param height the height for the fbo
     * @param options any options to pass into the fbo
     * @returns {THREE.WebGLRenderTarget}
     */
    static generateFbo(width=1024,height=1024,options){
        if(Capabilities.haveThreejs()){
            var defaults = {
                minFilter: THREE.NearestFilter,
                magFilter: THREE.NearestFilter,
                format: THREE.RGBAFormat,
                type: THREE.FloatType,
                stencilBuffer: false
            };

            if(options !== undefined){
                Object.assign(defaults,options);
            }

            return new THREE.WebGLRenderTarget(width,height,defaults);
        }
    }

    /**
     * Generates a pass-thru vertex shader
     * @returns {string}
     * @private
     */
    static createPassthruVertex(){
        //more threejs specific
        if(Capabilities.haveThreejs()){
            return [
                'varying vec2 vUv;',
                'void main() {',
                '  vUv = uv;',
                '  gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );',
                '}'
            ].join('\n');
        }
    }
}

export default GLUtils;