
import RenderBuffer from "./renderbuffer/RenderBuffer"
const glslify = require('glslify');

class ParticleSystem{
    constructor(renderer){


        var size = 20;
        this.buffer = new RenderBuffer(20,glslify("./shaders/particles/sim.glsl"),renderer,{
            phi:{
                type:'t',
                value:null
            },

            theta:{
                type:'t',
                value:null
            },

        });
        this.phiBuffer = new RenderBuffer(size,glslify('./shaders/particles/phiCalc.glsl'),renderer);
        this.thetaBuffer = new RenderBuffer(size,glslify('./shaders/particles/thetaCalc.glsl'),renderer);
        this.size = size;

        this._setOriginData();
        this._calculateThetaAndPhiSpeeds();
        this._buildRenderScene();
    }

    /**
     * Calculates a random value for deriving a increase in theta and phi
     * and encoders
     * @private
     */
    _calculateThetaAndPhiSpeeds(){
        var s2 = this.size * this.size;
        var size = this.size;
        var phiSpeeds = new Float32Array( s2 * 4 );
        var thetaSpeeds = new Float32Array( s2 * 4 );

        for( var i =0; i < phiSpeeds.length; i++ ){
            phiSpeeds[ i ] = (Math.random() - .5 ) * size;
            thetaSpeeds[ i ] = (Math.random() - .5 ) * size;
        }

        this.thetaSpeeds = new THREE.DataTexture(
            thetaSpeeds,
            this.size,
            this.size,
            THREE.RGBAFormat,
            THREE.FloatType
        );

        this.phiSpeeds = new THREE.DataTexture(
            phiSpeeds,
            this.size,
            this.size,
            THREE.RGBAFormat,
            THREE.FloatType
        );
        this.thetaSpeeds.minFilter =  THREE.NearestFilter;
        this.thetaSpeeds.magFilter = THREE.NearestFilter;
        this.thetaSpeeds.needsUpdate = true;

        this.phiSpeeds.minFilter =  THREE.NearestFilter;
        this.phiSpeeds.magFilter = THREE.NearestFilter;
        this.phiSpeeds.needsUpdate = true;

        this.thetaBuffer.setUniform('thetaSpeed',this.thetaSpeeds);
        this.phiBuffer.setUniform('phiSpeed',this.phiSpeeds);
    }

    _setOriginData(){
        var size = this.size;
        this.buffer.resetRand(18); // populate sphere data
        this.phiBuffer.resetRand(18) // populate phi data
        this.thetaBuffer.resetRand(18) // populate theta data

    }

    _randomRange(min, max) {
        return min + Math.random() * (max - min);
    }

    _random(min,max){
        return min + Math.random() * (max - min);
    }
    /**
     * Build the geometry and material we're gonna render
     * @private
     */
    _buildRenderScene(){
        var size = this.size;

        var geo = new THREE.BufferGeometry();
        var positions = new Float32Array(  size * size * 3 );
        var sizes = new Float32Array(size * size);
        for ( var i = 0, j = 0, l = positions.length / 3; i < l; i ++, j += 3 ) {
            positions[ j     ] = ( i % size ) / size;
            positions[ j + 1 ] = Math.floor( i / size ) / size;

            sizes[i] = Math.random() * 20.0;
        }
        var sizeA = new THREE.BufferAttribute(sizes,1);
        geo.addAttribute('particleSize',sizeA);
        var posA = new THREE.BufferAttribute( positions , 3 );
        geo.addAttribute( 'position', posA );



        var mat = new THREE.RawShaderMaterial({
            vertexShader:glslify('./shaders/particles/particle-v.glsl'),
            fragmentShader:glslify('./shaders/particles/particle-f.glsl'),
            uniforms:{
                time:{
                    type:'f',
                    value:0.0
                },
                map:{
                    type:'t',
                    value:null
                },
                resolution:{
                    type:'v2',
                    value:new THREE.Vector2(window.innerWidth,window.innerHeight)
                },
                noise:{
                    type:'t',
                    value:THREE.ImageUtils.loadTexture('/img/noise.png')
                },
                pTex:{
                    type:'t',
                    value:THREE.ImageUtils.loadTexture('/img/hdrLight.jpg')
                }
            }
        });
        this.renderMesh = new THREE.Points(geo,mat);
        this.renderMat = mat;
    }

    update(){
        this.renderMat.uniforms.time.value = performance.now() * 0.0001;
        this.renderMat.uniforms.map.value = this.buffer.getOutput();
        this.buffer.run();
        this.phiBuffer.run();
        this.thetaBuffer.run();
        this.buffer.updateUniform('theta',this.thetaBuffer.getOutput());
        this.buffer.updateUniform('phi',this.phiBuffer.getOutput());

    }

    addTo(scene){
        scene.add(this.renderMesh);
        return this;
    }
}

export default ParticleSystem;