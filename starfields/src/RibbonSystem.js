import Ribbon from "./Ribbon"
import RenderBuffer from "./renderbuffer/RenderBuffer"
const glslify = require('glslify');
import {createUniform} from "./framework/three/MaterialUtils"
class RibbonSystem{
    constructor(renderer,camera){
        this.camera = camera;
        this.ribbon = new Ribbon();
        var size = 20;

        this.phiBuffer = new RenderBuffer(size,glslify('./shaders/particles/phiCalc.glsl'),renderer);
        this.thetaBuffer = new RenderBuffer(size,glslify('./shaders/particles/thetaCalc.glsl'),renderer);
        this.size = size;

        this._setOriginData();
        this._calculateThetaAndPhiSpeeds();
        this._buildRenderScene();
    }

    /**
     * Calculates a random value for deriving a increase in theta and phi
     * and encoders
     * @private
     */
    _calculateThetaAndPhiSpeeds(){
        var s2 = this.size * this.size;
        var size = this.size;
        var phiSpeeds = new Float32Array( s2 * 4 );
        var thetaSpeeds = new Float32Array( s2 * 4 );

        for( var i =0; i < phiSpeeds.length; i++ ){
            phiSpeeds[ i ] = (Math.random() - .5 ) * size;
            thetaSpeeds[ i ] = (Math.random() - .5 ) * size;
        }

        this.thetaSpeeds = new THREE.DataTexture(
            thetaSpeeds,
            this.size,
            this.size,
            THREE.RGBAFormat,
            THREE.FloatType
        );

        this.phiSpeeds = new THREE.DataTexture(
            phiSpeeds,
            this.size,
            this.size,
            THREE.RGBAFormat,
            THREE.FloatType
        );
        this.thetaSpeeds.minFilter =  THREE.NearestFilter;
        this.thetaSpeeds.magFilter = THREE.NearestFilter;
        this.thetaSpeeds.needsUpdate = true;

        this.phiSpeeds.minFilter =  THREE.NearestFilter;
        this.phiSpeeds.magFilter = THREE.NearestFilter;
        this.phiSpeeds.needsUpdate = true;

        this.thetaBuffer.setUniform('thetaSpeed',this.thetaSpeeds);
        this.phiBuffer.setUniform('phiSpeed',this.phiSpeeds);
    }

    _setOriginData(){
        var size = this.size;
        this.phiBuffer.resetRand(18) // populate phi data
        this.thetaBuffer.resetRand(18) // populate theta data

    }

    _randomRange(min, max) {
        return min + Math.random() * (max - min);
    }

    _random(min,max){
        return min + Math.random() * (max - min);
    }
    /**
     * Build the geometry and material we're gonna render
     * @private
     */
    _buildRenderScene(){

        var planet = new THREE.IcosahedronGeometry(45,3);
        var self = this;
        var camera = this.camera;
        this.ribbon.setPath(planet);
        var resolution = new THREE.Vector2(window.innerWidth,window.innerHeight);


        var loader = new THREE.TextureLoader();

        this.ribbon.build({
            useMap: false,
            color: new THREE.Color( 0xff00ff ),
            opacity: 1.0,
            resolution: resolution,
            sizeAttenuation: false,
            lineWidth: 10.0,
            near: camera.near,
            far: camera.far
        });

        loader.load('/img/hdrLight.jpg',function(tex){
            self.ribbon.updateUniform('reflective',tex);
        });
    }

    update(){
        //this.ribbon.updateUniform('time',performance.now() * 0.0001);
        //this.ribbon.updateUniform('map',this.buffer.getOutput());

        this.phiBuffer.run();
        this.thetaBuffer.run();

        let mesh = this.ribbon.getMesh();
        mesh.rotation.x += 0.01;
        mesh.rotation.y += 0.01;

    }

    addTo(scene){
        this.ribbon.addTo(scene);
        return this;
    }
}

export default RibbonSystem;