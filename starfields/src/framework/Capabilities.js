/**
 * A basic class to sniff out various features of the user's browser
 */
class Capabilities {
    constructor(){}


    /**
     * Does a check to see if the browser supports webgl
     * @param returnContent a flag to indicate whether we want to return the resulting <canvas> element and rendering context.
     * default is false
     * @returns {*}
     */
    static hasWebGL(returnContent=false){
        var canWebGL = false;
        this.types = [
            "webgl2",
            "experimental-webgl2",
            "webgl",
            "experimental-webgl"
        ]

        var canvas = document.createElement("canvas");
        var ctx = null;
        for(var i = 0; i < this.types.length; ++i){
            ctx = canvas.getContext(this.types[i]);
            if(ctx !== null){
                canWebGL = true;
                break;
            }
        }

        if(!returnContent){
            return canWebGL;
        }else{
            return {
                domElement:canvas,
                gl:ctx
            }
        }
    }

    static haveThreejs(){
        if(window.THREE !== undefined){
            return true;
        }else{
            return false;
        }
    }

    /**
     * Does a general check via the useragent to
     * figure out if we're on a mobile browser
     */
    static isMobile(){
        let reg = new RegExp('Android|iPhone|iPad|Blackberry');
        let ua = navigator.userAgent;

        if(ua.search(reg) !== -1){
            return true;
        }else{
            return false;
        }
    }
}

export default Capabilities;