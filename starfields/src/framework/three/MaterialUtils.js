/**
 * Wrapper around setting a uniform value
 * @param name name of the uniform
 * @param value the value you want to set
 * @returns {ShaderMaterial}
 */
export function setUniform(shader,params) {
    let uniforms = shader.uniforms;
    if(uniforms.hasOwnProperty(params.name)){
        uniforms[params.name].value = params.value;
    }
    return shader;
}

export function getAttribLocation(renderer,attrib){
    let gl = renderer.getContext();
}

/**
 * Makes an attempt to create a Three.js useable uniform object based on the
 * value that you pass in
 * @param value the value you want to build a uniform for
 * @returns {{type: string, value: *}} a Object representing a uniform value that
 * can be inserted into a ShaderMaterial
 */
export function createUniform(value){
    var uniformType = '';

    // check for vector or texture types
    if(value instanceof THREE.Texture){
        uniformType = 't'
    }else if(value instanceof THREE.Vector2){
        uniformType = 'v2';
    }else if(value instanceof  THREE.Vector3){
        uniformType = 'v3'
    }else if(value instanceof THREE.Vector4){
        uniformType = 'v4'
    }else if(value instanceof THREE.Color){
        uniformType = 'c'
    }else{
        if(hasDecimalPlaces(value)){
            uniformType = 'f'
        }else{
            uniformType = 'i'
        }
    }

    //if the value is null, we assume it's a texture cause
    //pretty much everything else, you could just set some kind
    //of default value
    if(value === null){
        uniformType = 't';
    }

    //if the value is boolean, use 1s and 0s to represent
    if(value === false){
        uniformType = 'f';
        value = 0.0;
    }
    if(value === true){
        uniformType = 'f';
        value = 1.0;
    }

    /**
     * checks to see if theres a decimal place in a number
     * @param num
     * @returns {boolean}
     */
    function hasDecimalPlaces(num){
        var match = ('' + num).match(/(?:\.(\d+))?(?:[eE]([+-]?\d+))?$/);
        if(!match){
            return false;
        }else{
            return true;
        }
    }


    return {
        type:uniformType,
        value:value
    }
}