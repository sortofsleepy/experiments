/**
 * A simple Vector class
 */
class MVector {
    constructor(x=0,y=0,z=0){
        this.x = x;
        this.y = y;
        this.z = z;
    }

    /**
     * Adds two vectors together
     * @param vec another Vector object
     */
    add(vec){
        if(vec instanceof Vector){
            this.x += vec.x;
            this.y += vec.y;
            this.z += vec.z;
        }
    }

    /**
     * Subtracts a vector's values from this one
     * @param vec the other vector
     */
    subtract(vec){
        if(vec instanceof Vector){
            this.x -= vec.x;
            this.y -= vec.y;
            this.z -= vec.z;
        }
    }

    /**
     * Divides this vector by another
     * @param vec the other vector
     */
    divide(vec){
        if(vec instanceof Vector){
            this.x /= vec.x;
            this.y /= vec.y;
            this.z /= vec.z;
        }
    }

    /**
     * Normalizes the Vector's values
     */
    normalize(){
        var x = this.x;
        var y = this.y;
        var z = this.z;
        var len = x * x + y * y + z * z;
        if (len > 0) {
            len = 1 / Math.sqrt(len);
            this.x *= len;
            this.y *= len;
            this.z *= len;
        }
        return this;
    }

    /**
     * Calculates the dot product between this vec
     * and another one.
     * @param vec another vec
     */
    dot(vec){
        if(vec instanceof Vector){
            return this.x * vec.x + this.y * vec.y + this.z * vec.z;
        }
    }

    /**
     * Calculates the length of a vector
     * @returns {number} the length
     */
    length(){
        return Math.sqrt(this.x * this.x + this.y * this.y + this.z * this.z);
    }

    /**
     *  Divides the vector by a singular value.
     */
    divideScalar(value=1){
        this.x /= value;
        this.y /= value;
        this.z /= value;
        return this;
    }
    /**
     *  Multiplies the vector by a singular value.
     */
    multiplyScalar(value=1){
        this.x *= value;
        this.y *= value;
        this.z *= value;
        return this;
    }

    /**
     *  Adds a singular value onto the vector
     */
    addScalar(value=1){
        this.x += value;
        this.y += value;
        this.z += value;
        return this;
    }

    /**
     *  Subtracts the value from the vector.
     */
    subtractScalar(value=1){
        this.x -= value;
        this.y -= value;
        this.z -= value;
        return this;
    }

}

export default MVector;
