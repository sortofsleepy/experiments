/**
 * Utility class to handle asset loading.
 */
const rsvp = require('rsvp');
class Loader {
    constructor(onprogress,onload,onerror){
        this.onprogress = onprogress;
        this.onload = onload;
        this.onerror = onerror;

        //place to store images if no onload callback was specified
        if(this.onload === undefined){
            window.Loader_loadedImages = [];
        }

        this.queue = [];
    }

    addToQueue(filepath){
        this.queue.push(filepath);
    }

    load(){

    }


    /**
     * Loads one texture using the THREE.TextureLoader object
     * @param url the url to load
     * @param onload the callback when the image is loaded. Can optionally pass in a ShaderMaterial and will automatically set the first
     * texture type it encounters
     * @param onprogress the progress callback
     * @param onerror callback to use when a error is encoutered
     */
    static loadTexture(url,onload,onprogress,onerror){
        if(window.THREE === undefined){
            console.error("Cannot call loadTexture function as THREE.TextureLoader isn't available");
            return;
        }else{
            let loader = new THREE.TextureLoader();

            if(onload instanceof THREE.ShaderMaterial || onload instanceof THREE.RawShaderMaterial){
                loader.load(url,function(tex){
                    var uniforms = onload.uniforms;
                    for(var i in uniforms){
                        if(uniforms[i].type === 't'){
                            uniforms[i].value = tex;
                        }
                    }
                },onprogress,onerror);
            }else{
                loader.load(url,onload,onprogress,onerror);
            }

        }
    }

    /**
     * Loads a manifest of textures using the THREE.TextureLoader
     * @param urls a array of objects consisting of two components
     * 1. id - the name of the id you want to use to identify this texture
     * 2. path - the path to the source for the texture
     * @param progressCb a callback to fire for the progress event during loading
     * @returns {*} returns a promise
     */
    static loadTextures(urls,progressCb){
        let loaded = [];
        let loader = new THREE.TextureLoader();
        let promise = null;

        //check to make sure we have an array
        if(urls instanceof Array){
            promise = new rsvp.Promise(function(resolve,reject){
                let count = urls.length
                for(var i = 0; i < count; ++i){
                    let name = urls[i].id;

                    //start the loader.
                    loader.load(urls[i].path,function(tex){
                        tex["TEXTURE_ID"] = name;
                        loaded.push(tex);

                    },progressCb,function(e){
                        reject({
                            res:"ERROR",
                            message:`Unable to load texture ${name}: ` + e
                        })
                    })
                }

                // set a timer to wait till all the images are loaded, then
                // we resolve the promise.
                var timer = setInterval(function(){
                    if(loaded.length === count){
                        resolve(loaded);
                        clearInterval(timer);

                    }
                })



            });
        }
        return promise;
    }


}

export default Loader;
