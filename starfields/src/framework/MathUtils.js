//various useful general functions taken from a variety of places.
// https://www.airtightinteractive.com/demos/js/noise-ribbons/lib/atutil.js
// https://github.com/yiwenl/Christmas_Experiment_2015/blob/master/src/js/SceneApp.js


export function randomRange(min, max) {
    return min + Math.random() * (max - min);
}

export function randomInt(min,max){
    return Math.floor(min + Math.random() * (max - min + 1));
}

export function map(value, min1, max1, min2, max2) {
    return lerp( norm(value, min1, max1), min2, max2);
}

export function lerp(val,min,max){
    return min + (max -min) * value;
}

export function norm(value,min,max){
    return (value - min) / (max - min);
}

/**
 * Converts degrees to radians
 * @param deg the value in degrees to convert
 * @returns {number} the value in radians
 */
export function toRadians(deg){
    return (deg * Math.PI) / 180;
}
/**
 * Convert a radian value to degrees
 * @param rad a radian value
 * @returns {number} the value in degrees
 */
export function toDegrees(rad){
    return (rad * 180 / Math.PI);
}

/**
 * Generates a random vector 3 within a range. Will return a THREE.Vector3 if available, otherwise
 * returns a array.
 * @param range
 * @returns {*}
 */
export function randVec3(range){
    if(window.THREE !== undefined){
        return new THREE.Vector3(randomRange(-range,range),randomRange(-range,range),randomRange(-range,range));
    }else{
        return [randomRange(-range,range),randomRange(-range,range),randomRange(-range,range)];
    }
}

export function slerp(x,y,p){
    return x + (y - x) * p;
}

/**
 * Normalizes a vector to values between 0 and 1
 * @param vector the vector to normalize
 * @returns {*[]}
 */
export function normalize(vector){
    if(vector instanceof THREE.Vector3 || vector.hasOwnProperty("x")){
        let x = vector.x;
        let y = vector.y;
        let z = vector.z;

        let len = x * x + y * y + z * z;
        if(len > 0){
            len = 1 / Math.sqrt(len);
            x *= len;
            y *= len;
            z *= len;
        }

        return [x,y,z];
    }
}