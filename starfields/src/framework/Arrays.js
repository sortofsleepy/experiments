/**
 * "Flattens" an array of arrays(or array of vectors, etc) so that it is useable in WebGL.
 * @param array the array to flatten
 * @param returnRegular if this is false, will return a typed array, defaulting to Float32Array. If another type is desired, pass in the shorthand
 * "uint16", "uint8" etc.
 * @returns {*}
 */
export function flattenArray(array,returnRegular=false){
    var len = array.length;
    var newArr = [];

    if(array instanceof Array){
        for(var i = 0; i < len; ++i){
            let arr = array[i];

            //if we have a regular array within a arry
            if(arr instanceof Array){
                if(arr.length > 2){
                    newArr.push(arr[0],arr[1],arr[2]);
                }else{
                    newArr.push(arr[0],arr[1]);
                }
            }

            if(arr instanceof THREE.Vector3){
                newArr.push(arr.x,arr.y,arr.z);
            }

            if(arr instanceof THREE.Vector2){
                newArr.push(arr.x,arr.y);
            }

        }
    }


    if(!returnRegular){
        switch (returnRegular){
            case "uint8":
                return new Uint8Array(newArr);
                break;

            case "uint16":
                return new Uint16Array(newArr);
                break;
            case "float32":
                return new Float32Array(newArr);
            break;

            default:
                return new Float32Array(newArr);
            break;
        }
    }else{
        return newArr
    }

}

/**
 * Converts a HTMLCollection object to a regular array
 * @param collection the colelction to convert
 * @returns {Array.<T>}
 */
export function htmlCollectionToArray(collection){
    return [].slice.call(collection);
}

/**
 * Looks for a item inside of a array
 * @param array the array to search through
 * @param item the value you're looking for
 * @returns {*}
 */
export function findInArray(array,item){
    let val = null;
    let len = array.length;
    for(var i = 0; i < len; ++i){
        if(array[i] === item){
            val = array[i];
        }
    }
    return val;
}