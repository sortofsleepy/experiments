const glslify = require('glslify')
import {createUniform} from "./framework/three/MaterialUtils"
/**
 * A slight variation of Jaume Sanchez's (aka @thespite) THREE.MeshLine
 * that supports instanced lines. Also bakes in Three.MeshLineMaterial with glslify
 * TODO add dynamic attribs per instance
 * https://github.com/spite/THREE.MeshLine
 */
class Ribbon {
    constructor(params){
        this.positions = [];
        this.previous = [];
        this.next = [];
        this.side = [];
        this.width = [];
        this.indices_array = [];
        this.uvs = [];

        this.geometry = new THREE.InstancedBufferGeometry();
        this.dashArray = [];
        this.params = {
            reflective:{

                type:'t',
                value:false
            },
            lineWidth:{
                type:'f',
                value:5.0
            },
            map:{
                type:'t',
                value:null
            },
            useMap:{
                type:'f',
                value:0.0
            },
            color:{
                type:'c',
                value:new THREE.Color(0xffffff)
            },
            opacity:{
                type:'f',
                value:1.0
            },
            resolution:{
                type:'v2',
                value:new THREE.Vector2(1,1)
            },
            sizeAttenuation:{
                type:'f',
                value:1.0
            },
            near:{
                type:'f',
                value:1.0
            },
            far:{
                type:'f',
                value:1.0
            },
            dashArray: {
                type: 'v2',
                value: new THREE.Vector2( this.dashArray[ 0 ], this.dashArray[ 1 ] )
            },
            useDash: { type: 'f', value: 0.0 }
        }
    }

    /**
     * Sets a custom fragment shader for the line
     * @param fragsource the source for the shader
     * @returns {Ribbon}
     */
    setFragment(fragsource){

        //if the material doesn't exist yet, store source for later
        if(this.material === undefined){
            this.customFrag = fragsource;
        }else{
            //otherwise overwrite the shader thats there
            this.material.fragmentShader = fragsource;
        }
        return this;
    }

    setVertex(fragsource){
        if(this.material === undefined){
            this.customVert = vertsource;
        }else{
            this.material.vertexShader = vertsource
        }
        return this;
    }



    /**
     * Adds a additional uniform to the material ontop of all of the standard ones
     * @param uniformObj a Object likeso
     * name:{type:<type>,value:<value>}
     * @returns {Ribbon}
     */
    addUniform(uniformObj){
        if(this.material !== undefined){
            Object.assign(this.material.uniforms,uniformObj);
        }else{
            console.error("can't add any additional uniforms until material is built");
        }
        return this;
    }

    /**
     * Builds the line
     * @param parameters any parameters for the material that you'd like to overrite
     * @param instances the number of instances
     * @param instancePositionSet a callback function to set the instance positions. Your function gets
     * passed the THREE.InstancedBufferAttribute to manipulate, and needs to return that in order for things to work.
     * @returns {THREE.Mesh|*}
     */
    build(parameters,instances=0,instancePositionSet=null){
        parameters = parameters !== undefined ? parameters : {}
        //update the params with new values if necessary
        for(var i in this.params){
            if(parameters.hasOwnProperty(i)) {
                this.params[i].value = parameters[i];
            }
        }

        //loop through and add any new params if needed
        for(var i in parameters){
            this.params[i] = createUniform(parameters[i]);
        }

        console.log(this.params);

        //account for any custom shaders if needbe
        var fragSource = ""
        var vertSource = "";
        if(this.customFrag !== undefined){
            fragSource = this.customFrag;
        }else{
            fragSource = glslify('./shaders/line-f.glsl');
        }

        if(this.customVert !== undefined){
            vertSource = this.customVert;
        }else{
            vertSource = glslify('./shaders/line-v.glsl');
        }


        var params = this.params;


        //build the material
        var material = new THREE.RawShaderMaterial( {
            uniforms:params,
            vertexShader: vertSource,
            fragmentShader: fragSource
        });

        if(instances > 0){
            var offsets = new THREE.InstancedBufferAttribute( new Float32Array( instances * 3 ), 3, 1 );

            //with no callback, just randomly scatter everything
            if(instancePositionSet === null){
                //just randomly scatter everything
                var vector = new THREE.Vector4();
                for ( var i = 0, ul = offsets.count; i < ul; i++ ) {
                    var x = Math.random() * 100 - 50;
                    var y = Math.random() * 100 - 50;
                    var z = Math.random() * 100 - 50;
                    vector.set( x, y, z, 0 ).normalize();
                    // move out at least 5 units from center in current direction
                    offsets.setXYZ( i, x + vector.x * 5, y + vector.y * 5, z + vector.z * 5 );
                }
            }else{
                offsets = instancePositionSet(offsets);
            }

            // check to make sure offsets really is a InstancedBufferAttribute, then set
            if(offsets instanceof THREE.InstancedBufferAttribute){
                this.geometry.addAttribute('offsetPosition',offsets);
            }
        }


        this.material = material;
        this.mesh = new THREE.Mesh(this.geometry,this.material);
        return this.mesh;

    }
    addTo(scene){
        scene.add(this.mesh);
    }

    updateUniform(key,value){
        let uniforms = this.material.uniforms;
        uniforms[key].value = value;
    }

    getMesh(){
        return this.mesh;
    }

    /**
     * Gets the vertices for all of the ribbons
     * @returns {*|string}
     */
    getVertices(){
        if(this.mesh !== undefined){
            return this.mesh.geometry.getAttribute('position').array;
        }
    }

    /**
     * Sets the path of the function
     * @param g can either be a Float32Array or a callback function. The callback function is
     * expected to return a array
     */
    setPath(g){
        if(g instanceof Float32Array){
            for( var j = 0; j < g.length; j += 3 ) {
                this.positions.push( g[ j ], g[ j + 1 ], g[ j + 2 ] );
                this.positions.push( g[ j ], g[ j + 1 ], g[ j + 2 ] );
            }
        }else if( g instanceof THREE.Geometry ) {
            for( var j = 0; j < g.vertices.length; j++ ) {
                var v = g.vertices[ j ];
                this.positions.push( v.x, v.y, v.z );
                this.positions.push( v.x, v.y, v.z );
            }
        }else if(g instanceof Function){
            let array = g();
            for( var j = 0; j < array.length; j += 3 ) {
                this.positions.push( array[ j ], array[ j + 1 ], array[ j + 2 ] );
                this.positions.push( array[ j ], array[ j + 1 ], array[ j + 2 ] );
            }
        }
        this.process();
    }

    compareV3(a,b){
        var aa = a * 6;
        var ab = b * 6;
        return ( this.positions[ aa ] === this.positions[ ab ] ) && ( this.positions[ aa + 1 ] === this.positions[ ab + 1 ] ) && ( this.positions[ aa + 2 ] === this.positions[ ab + 2 ] );

    }

    copyV3(a){
        var aa = a * 6;
        return [ this.positions[ aa ], this.positions[ aa + 1 ], this.positions[ aa + 2 ] ];
    }

    process(){
        var l = this.positions.length / 6;

        this.previous = [];
        this.next = [];
        this.side = [];
        this.width = [];
        this.indices_array = [];
        this.uvs = [];

        for( var j = 0; j < l; j++ ) {
            this.side.push( 1 );
            this.side.push( -1 );
        }

        var w = 1
        for( var j = 0; j < l; j++ ) {
            this.width.push( w );
            this.width.push( w );
        }

        for( var j = 0; j < l; j++ ) {
            this.uvs.push( j / ( l - 1 ), 0 );
            this.uvs.push( j / ( l - 1 ), 1 );
        }

        var v;

        if( this.compareV3( 0, l - 1 ) ){
            v = this.copyV3( l - 2 );
        } else {
            v = this.copyV3( 0 );
        }

        this.previous.push( v[ 0 ], v[ 1 ], v[ 2 ] );
        this.previous.push( v[ 0 ], v[ 1 ], v[ 2 ] );

        for( var j = 0; j < l - 1; j++ ) {
            v = this.copyV3( j );
            this.previous.push( v[ 0 ], v[ 1 ], v[ 2 ] );
            this.previous.push( v[ 0 ], v[ 1 ], v[ 2 ] );
        }

        for( var j = 1; j < l; j++ ) {
            v = this.copyV3( j );
            this.next.push( v[ 0 ], v[ 1 ], v[ 2 ] );
            this.next.push( v[ 0 ], v[ 1 ], v[ 2 ] );
        }

        if( this.compareV3( l - 1, 0 ) ){
            v = this.copyV3( 1 );
        } else {
            v = this.copyV3( l - 1 );
        }
        this.next.push( v[ 0 ], v[ 1 ], v[ 2 ] );
        this.next.push( v[ 0 ], v[ 1 ], v[ 2 ] );

        for( var j = 0; j < l - 1; j++ ) {
            var n = j * 2;
            this.indices_array.push( n, n + 1, n + 2 );
            this.indices_array.push( n + 2, n + 1, n + 3 );
        }

        this.attributes = {
            position: new THREE.BufferAttribute( new Float32Array( this.positions ), 3 ),
            previous: new THREE.BufferAttribute( new Float32Array( this.previous ), 3 ),
            next: new THREE.BufferAttribute( new Float32Array( this.next ), 3 ),
            side: new THREE.BufferAttribute( new Float32Array( this.side ), 1 ),
            width: new THREE.BufferAttribute( new Float32Array( this.width ), 1 ),
            uv: new THREE.BufferAttribute( new Float32Array( this.uvs ), 2 ),
            index: new THREE.BufferAttribute( new Uint16Array( this.indices_array ), 1 )
        }

        this.geometry.addAttribute( 'position', this.attributes.position );
        this.geometry.addAttribute( 'previous', this.attributes.previous );
        this.geometry.addAttribute( 'next', this.attributes.next );
        this.geometry.addAttribute( 'side', this.attributes.side );
        this.geometry.addAttribute( 'width', this.attributes.width );
        this.geometry.addAttribute( 'uv', this.attributes.uv );

        this.geometry.setIndex( this.attributes.index );
    }
}

export default Ribbon;