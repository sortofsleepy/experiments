import Loader from "./framework/Loader"
import {flattenArray} from "./framework/Arrays"
const glslify = require('glslify');

class EnvMap {
    constructor(scene){
        let self = this;
        this.planes = [];

        Loader.loadTextures([
            {
                id:"stars",
                path:"/img/stars.jpg"
            }
        ]).then(function(textures){
            self._buildWalls(textures,scene);
        });

    }

    /**
     * Builds our cornelish box
     * @param tex
     * @private
     */
    _buildWalls(textures,scene){
        let stars = null;
        let matcap = null;

        textures.forEach(texture => {
            if(texture.TEXTURE_ID === "matcap"){
                matcap = texture;
            }else{
                stars = texture;
            }
        });

        var geo = new THREE.BufferGeometry();

        var material = new THREE.ShaderMaterial({
            vertexShader:glslify('./shaders/cornel-v.glsl'),
            fragmentShader:glslify('./shaders/cornel-f.glsl'),
            uniforms:{
                stars:{
                    type:'t',
                    value:stars
                }
            }
        })

        var numSegments = 24;
        var size = 1800;
        var positions = [];
        var coords    = [];
        var indices   = [];
        var index     = 0;
        var gapUV     = 1/numSegments;

        var getPosition = function(i, j, isNormal) {	//	rx : -90 ~ 90 , ry : 0 ~ 360
            isNormal = isNormal === undefined ? false : isNormal;
            var rx = i/numSegments * Math.PI - Math.PI * 0.5;
            var ry = j/numSegments * Math.PI * 2;
            var r = isNormal ? 1 : size;
            var pos = [];
            pos[1] = Math.sin(rx) * r;
            var t = Math.cos(rx) * r;
            pos[0] = Math.cos(ry) * t;
            pos[2] = Math.sin(ry) * t;
            return pos;
        };




        for(var i=0; i<numSegments; i++) {
            for(var j=0; j<numSegments; j++) {
                positions.push(getPosition(i, j));
                positions.push(getPosition(i+1, j));
                positions.push(getPosition(i+1, j+1));
                positions.push(getPosition(i, j+1));
                var u = j/numSegments;
                var v = i/numSegments;


                coords.push([1.0 - u, v]);
                coords.push([1.0 - u, v+gapUV]);
                coords.push([1.0 - u - gapUV, v+gapUV]);
                coords.push([1.0 - u - gapUV, v]);

                indices.push(index*4 + 3);
                indices.push(index*4 + 2);
                indices.push(index*4 + 0);
                indices.push(index*4 + 2);
                indices.push(index*4 + 1);
                indices.push(index*4 + 0);

                index++;
            }
        }


        positions = flattenArray(positions);
        coords = flattenArray(coords);
        indices = new Uint16Array(indices);

        var positionBuffer = new THREE.BufferAttribute(positions,3);
        var coordBuffer = new THREE.BufferAttribute(coords,2);
        var indexBuffer = new THREE.BufferAttribute(indices,1);


        geo.addAttribute("position",positionBuffer);
        geo.addAttribute("uv",coordBuffer);
        geo.setIndex(indexBuffer);

        var mesh = new THREE.Mesh(geo,material);
        scene.add(mesh);


    }

    addTo(scene){
        this.planes.forEach(plane => {
            scene.add(plane);
        });
    }
}

export default EnvMap;