const glslify = require('glslify');
import RenderBuffer from "./src/renderbuffer/RenderBuffer"

var scene , camera , renderer, clock , controls;
var mat;
var SIZE = 128;
var simulation;

var simulationUniforms = {

    timer:{ type:"f" , value: 0 },
    noiseSize: { type:"f" , value: .1 }

}

var renderUniforms = {

    t_pos:{ type:"t" , value: null }

}



init();
animate();

function init(){

    /*

     Default threejs stuff!

     */
    scene = new THREE.Scene();

    var ar = window.innerWidth / window.innerHeight;

    camera = new THREE.PerspectiveCamera( 75, ar , 1, 1000 );
    camera.position.z = 100;

    renderer = new THREE.WebGLRenderer();
    renderer.setSize( window.innerWidth, window.innerHeight );

    document.body.appendChild( renderer.domElement );


    //controls = new THREE.TrackballControls( camera );
    clock = new THREE.Clock();

    let simShader = glslify('./src/shaders/curl.glsl');

    simulation = new RenderBuffer(SIZE,simShader,renderer,simulationUniforms);

    var geo = createLookupGeometry( SIZE );

    mat = new THREE.ShaderMaterial({
        uniforms: renderUniforms,
        vertexShader: glslify('./src/shaders/crender-v.glsl'),
        fragmentShader: glslify('./src/shaders/crender-f.glsl'),

    });

    var particles = new THREE.Points( geo , mat );
    particles.frustumCulled = false;
    scene.add( particles );

    simulation.resetRand( 5 );

}

animate();
function animate(){

    requestAnimationFrame( animate );
    mat.uniforms.t_pos.value = simulation.getOutput();
    //simulationUniforms.dT.value = clock.getDelta();
    simulation.update();

    // controls.update();

    renderer.render( scene , camera );

}


function createLookupGeometry( size ){

    var geo = new THREE.BufferGeometry();
    var positions = new Float32Array(  size * size * 3 );

    for ( var i = 0, j = 0, l = positions.length / 3; i < l; i ++, j += 3 ) {

        positions[ j     ] = ( i % size ) / size;
        positions[ j + 1 ] = Math.floor( i / size ) / size;

    }

    var posA = new THREE.BufferAttribute( positions , 3 );
    geo.addAttribute( 'position', posA );

    return geo;

}